# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.9)
# Database: dixa
# Generation Time: 2012-07-10 06:03:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table loader_log
# ------------------------------------------------------------

CREATE TABLE `loader_log` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `starttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number_of_processes` int(10) NOT NULL,
  `submission_ids` varchar(250) DEFAULT '',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `loader_log` WRITE;
/*!40000 ALTER TABLE `loader_log` DISABLE KEYS */;

INSERT INTO `loader_log` (`job_id`, `starttime`, `endtime`, `number_of_processes`, `submission_ids`)
VALUES
	(1,'2012-07-06 09:16:02','2012-07-06 09:16:02',1,NULL),
	(2,'2012-07-06 09:16:51','2012-07-06 09:16:51',1,NULL),
	(3,'2012-07-06 09:17:12','2012-07-06 09:17:12',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(4,'2012-07-09 12:08:27','2012-07-09 12:08:27',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(5,'2012-07-09 12:42:01','2012-07-09 12:42:02',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(6,'2012-07-09 12:44:27','2012-07-09 12:44:28',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(7,'2012-07-09 12:45:05','2012-07-09 12:45:05',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(8,'2012-07-09 12:45:27','2012-07-09 12:45:28',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(9,'2012-07-09 12:46:04','2012-07-09 12:46:05',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(10,'2012-07-09 12:47:16','2012-07-09 12:47:17',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(11,'2012-07-09 12:48:05','2012-07-09 12:48:06',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(12,'2012-07-09 17:04:06','2012-07-09 17:04:07',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(13,'2012-07-09 17:05:31','2012-07-09 17:05:31',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(14,'2012-07-09 17:06:00','2012-07-09 17:06:01',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp'),
	(15,'2012-07-09 17:06:48','2012-07-09 17:06:49',1,'¬í\0ur\0[Ljava.lang.String;­ÒVçé{G\0\0xp\0\0\0\npppppppppp');

/*!40000 ALTER TABLE `loader_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table repository
# ------------------------------------------------------------

DROP TABLE IF EXISTS `repository`;

CREATE TABLE `repository` (
  `rep_id` int(10) NOT NULL,
  `rep_name` varchar(250) NOT NULL,
  `desc` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `repository` WRITE;
/*!40000 ALTER TABLE `repository` DISABLE KEYS */;

INSERT INTO `repository` (`rep_id`, `rep_name`, `desc`, `url`)
VALUES
	(1,'test rep',NULL,NULL);

/*!40000 ALTER TABLE `repository` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table submission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `submission`;

CREATE TABLE `submission` (
  `submission_id` varchar(10) NOT NULL DEFAULT '',
  `submission_date` datetime DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `submitter` varchar(100) DEFAULT '',
  `number_of_files` int(10) DEFAULT NULL,
  PRIMARY KEY (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;

INSERT INTO `submission` (`submission_id`, `submission_date`, `path`, `submitter`, `number_of_files`)
VALUES
	('SUB_1','2012-06-27 00:00:00',NULL,'submitter_test',4),
	('SUB_2','2012-06-27 00:00:00',NULL,'submitter_test',4),
	('SUB_3','2012-06-27 00:00:00',NULL,'submitter_test',4),
	('SUB_4','2012-07-09 00:00:00',NULL,'submitter_test',4);

/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table submission_composition
# ------------------------------------------------------------

DROP TABLE IF EXISTS `submission_composition`;

CREATE TABLE `submission_composition` (
  `file_name` varchar(100) NOT NULL,
  `submission_id` varchar(10) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `md5` varchar(33) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`file_name`),
  KEY `fk_submission` (`submission_id`),
  CONSTRAINT `fk_submission` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `submission_composition` WRITE;
/*!40000 ALTER TABLE `submission_composition` DISABLE KEYS */;

INSERT INTO `submission_composition` (`file_name`, `submission_id`, `creation_date`, `md5`, `status`)
VALUES
	('test file 1 copy 2.txt','SUB_3','2012-06-27 00:00:00','fb59f89c423f53d28ae3db39e64c15d3',1),
	('test file 1 copy 3.txt','SUB_3','2012-06-27 00:00:00','fb59f89c423f53d28ae3db39e64c15d3',1),
	('test file 1 copy.txt','SUB_3','2012-06-27 00:00:00','fb59f89c423f53d28ae3db39e64c15d3',1),
	('test file 1.rtf','SUB_4','2012-07-09 00:00:00','1e1a60c568270c7b5acb46f25c8b7663',1),
	('test file 1.txt','SUB_3','2012-06-27 00:00:00','fb59f89c423f53d28ae3db39e64c15d3',1);

/*!40000 ALTER TABLE `submission_composition` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
