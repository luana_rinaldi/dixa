/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table loader_log
# ------------------------------------------------------------

CREATE TABLE `loader_log` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `starttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number_of_processes` int(10) NOT NULL,
  `dixa_ids` varchar(250) DEFAULT '',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table repository
# ------------------------------------------------------------

CREATE TABLE `repository` (
  `rep_id` int(10) NOT NULL,
  `rep_name` varchar(250) NOT NULL,
  `desc` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table submission
# ------------------------------------------------------------

CREATE TABLE `submission` (
  `dixa_id` varchar(10) NOT NULL,
  `ext_id` varchar(10) NOT NULL,
  `rep_id` int(10) NOT NULL,
  `submission_date` datetime DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `data_file` varchar(250) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `submitter` varchar(100) DEFAULT '',
  `number_of_files` int(10) DEFAULT NULL,
  PRIMARY KEY (`dixa_id`),
  KEY `rep_id` (`rep_id`),
  CONSTRAINT `submission_ibfk_1` FOREIGN KEY (`rep_id`) REFERENCES `repository` (`rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
