package xml;

import java.util.HashMap;
import java.util.Map;

import models.DBProperties;
import models.FTPProperties;
import models.Property;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.xml.internal.ws.util.xml.NodeListIterator;

import exceptions.XMLException;

public class FTPConfigParser extends ConfigParser{
	private static Logger log = Logger.getLogger(FTPConfigParser.class); 
	
	
	public FTPConfigParser() throws Exception{
		super.tagName = ("dixa:ftp"); 
		this.init();
		
	}
	protected void init() throws Exception{
		try{
			log.debug("Starting XML parsing"); 
			super.init();
			super.prop = parseProperties(); 
		}catch(XMLException xe){
			log.error(xe.getMessage()); 
			throw new Exception(); 
		} 
	}
	@Override
	protected Property parseProperties() throws Exception{
		
		try{
			Element analyzersNode = (Element)super.xml.getElementsByTagName("dixa:analyzers").item(0);
			NodeList nodelist2 = analyzersNode.getChildNodes();
			NodeListIterator it2 = new NodeListIterator(nodelist2);
			Map<String,String> analyzers = null; 
			while (it2.hasNext()){
				Node node =(Node) it2.next(); 
				NamedNodeMap nnm2 = node.getAttributes();
				if (node.getNodeName().equals("dixa:analyzer")){
					analyzers = new HashMap<String,String>();
					String name= nnm2.getNamedItem("name").getNodeValue();
					String folder = nnm2.getNamedItem("folder").getNodeValue();
					analyzers.put(name, folder);
				}
			}
			
			Element ftpsNode = (Element)super.xml.getElementsByTagName(super.tagName).item(0); 
			 
			NamedNodeMap nnm = ftpsNode.getAttributes();
			String realPath = nnm.getNamedItem("realpath").getNodeValue(); 
			NodeList nodelist = ftpsNode.getChildNodes();
			
			String up_folder = null; 
			String arch_folder = null; 
			String newsub_folder = null; 
			String load_folder = null;
			String sync_folder = null; 
			String result_folder = null; 
			String load_newresult = null; 
			String load_archive = null; 
			NodeListIterator it = new NodeListIterator(nodelist);
			while (it.hasNext()){
				Node node =(Node) it.next(); 
				NamedNodeMap nnm2 = node.getAttributes();

				if (node.getNodeName().equals("dixa:upload")){
					up_folder = nnm2.getNamedItem("upload_f").getNodeValue();
					arch_folder = nnm2.getNamedItem("archive_f").getNodeValue();
					newsub_folder = nnm2.getNamedItem("newsub_f").getNodeValue();
				}else if (node.getNodeName().equals("dixa:load")){
					load_folder = nnm2.getNamedItem("load_f").getNodeValue();
					sync_folder = nnm2.getNamedItem("sync_f").getNodeValue();
					result_folder = nnm2.getNamedItem("results_f").getNodeValue();
					load_newresult = nnm2.getNamedItem("new_results_f").getNodeValue();
					load_archive = nnm2.getNamedItem("archive_f").getNodeValue();
					
				}
			}
			

			super.prop = new FTPProperties(realPath,up_folder,arch_folder,newsub_folder,
											load_folder,sync_folder,result_folder, 
											load_newresult,load_archive, analyzers);
		}catch(Exception e){
			throw e; 
		}
		return super.prop; 
	}
	
	/**
	 * @return DBProperties
	 * @throws Exception (XMLException) 
	 */
	@Override
	public  Property getProperties() throws Exception{
		if (super.prop == null){
			
			try{
				super.prop = this.parseProperties();
				log.debug("FTP Parsed");
			}catch(Exception xe){
				log.error(xe.getMessage()); 
				throw new XMLException(xe.getMessage()); 
			} 
		}
		return super.prop; 
	}

}
