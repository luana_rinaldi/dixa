package xml;

/***
 * @name DBConfig Parser: Parses the DB configuration file (in XML) 
 * @version 1.0
 * @author: Luana Rinaldi
 */


import models.DBProperties;
import models.Property;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import exceptions.XMLException;

public class DBConfigParser extends ConfigParser{
	private static Logger log = Logger.getLogger(DBConfigParser.class); 
	
	
	public DBConfigParser() throws Exception{
		super.tagName = ("dixa:db"); 
		this.init();
		
	}
	protected void init() throws Exception{
		try{
			log.debug("Starting XML parsing"); 
			super.init();
			super.prop = parseProperties(); 
		}catch(XMLException xe){
			log.error(xe.getMessage()); 
			throw new Exception(); 
		} 
	}
	@Override
	protected Property parseProperties() throws Exception{
		Property dbProp = null;
		try{
			Element dbPropNode = (Element)super.xml.getElementsByTagName(super.tagName).item(0); 
			 
			NamedNodeMap nnm = dbPropNode.getAttributes();
			String alias = nnm.getNamedItem("alias").getNodeValue(); 
			String jdbcDriver = nnm.getNamedItem("jdbcDriver").getNodeValue();
			String uri = nnm.getNamedItem("databaseUri").getNodeValue();
			String username = nnm.getNamedItem("userName").getNodeValue();
			String password = nnm.getNamedItem("password").getNodeValue();
			String dbName =nnm.getNamedItem("userName").getNodeValue();
			String databaseUri = uri; 
			dbProp = new DBProperties(alias, jdbcDriver, databaseUri, username, password); 
			log.debug(dbProp.toString()); 
		}catch(Exception e){
			throw e; 
		}
		return dbProp; 
	}
	
	/**
	 * @return DBProperties
	 * @throws Exception (XMLException) 
	 */
	@Override
	public  Property getProperties() throws Exception{
		if (super.prop == null){
			try{
				super.prop = this.parseProperties();
			}catch(Exception xe){
				log.error(xe.getMessage()); 
				throw new XMLException(xe.getMessage()); 
			} 
		}
		return super.prop; 
	}




	
	
	

}
