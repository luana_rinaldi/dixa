package xml;

/***
 * @name XMLParser
 * @version 1.0
 * @author: Luana Rinaldi
 */

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import exceptions.XMLException;

public class XMLParser {

    private static Logger log = Logger.getLogger(XMLParser.class); 
	    
    /**
     * init Parser
     * @param xmlPath
     */
	protected Document init(String xmlPath) throws XMLException{
		log.debug("Init controller parser"); 
		File f = new File(xmlPath);
		Document xml = null; 
		if(f.exists()){
			DocumentBuilder builder = null;
			try {
				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
				builderFactory.setNamespaceAware(true);
				builder= builderFactory.newDocumentBuilder();
				log.debug("Starting parse XML file");
				xml = builder.parse(f);
				log.debug("Root element " + xml.getDocumentElement().getNodeName());
				//XMLValidator.validate(xml, Constant.XSD_SCHEMA); 
				
			} catch (ParserConfigurationException e) {
				log.fatal("Error in parsing",e);
				throw new XMLException(e.getCause()); 
			} catch (SAXException e) {
				log.fatal("Error SAX",e); 
				throw new XMLException(e.getCause());
			} catch (IOException e) {
				log.fatal("Error reading XML file",e); 
				throw new XMLException(e.getCause());
			} 
		}
		return xml; 
	}
	
    /**
     * init Parser
     * @param NodeList nodeList
     * @param String item: Item to look for
     * @return String value: value of the given item
     */
	protected static String getAttribute(NodeList nodeList, String item )throws Exception
	{
		String value=""; 
		
		for(int c=0; c<nodeList.getLength(); c++)
		{
			Element element = (Element)nodeList.item(c); 
			
			/*getAttributes from the element */
			NamedNodeMap attribute = element.getAttributes();

			/*get ttribute's value*/
			value= attribute.getNamedItem(item).getNodeValue(); 						
		}
		return value; 
	}
	
}
