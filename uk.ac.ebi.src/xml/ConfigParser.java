package xml;

import models.DBProperties;
import models.Property;

import org.w3c.dom.Document;


import setup.Constant;
import exceptions.XMLException;

public abstract class ConfigParser {
	//parsed Document
		protected static Document xml; 
		
		//path to the XML configuration file
		protected static String xmlFilePath = Constant.XML_CONFIG_FILE;
		
		//Properties object
		public Property prop; 
		
		protected String tagName; 
		
		protected void init() throws Exception{
			try{
				XMLParser parser = new XMLParser(); 
				xml = parser.init(ConfigParser.xmlFilePath); 
			}catch(XMLException xe){
				throw new Exception(); 
			} 
		}
		
		protected abstract Property parseProperties()throws Exception; 
		
		public abstract Property getProperties() throws Exception;
}
