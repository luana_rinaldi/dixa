package managers;

import org.apache.log4j.Logger;

import db.DAOSubmission;

public class IDGenerator {

	private static Logger log = Logger.getLogger(IDGenerator.class); 
	
	public static String getNewID(String prefix){
		
		String next = null; 
		try{
			int id = 0;
			String lastID = DAOSubmission.getLastID(prefix); 
			if (lastID != null){
				String[] res=lastID.split("_");
				id = Integer.parseInt(res[1]);	
			}			
			next= prefix+"_"+(id+1);
		}catch(Exception e){
			log.error(e.getMessage());
		}
		return next;
	}
}
