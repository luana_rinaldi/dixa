package setup;
/***
 * @name Constants
 * @version 1.0
 * @author: Luana Rinaldi
 */


public class Constant {
	public final static String LOG4J_INIT = "uk.ac.ebi.config/log4j.xml";
	//public final static String PROPERTIES_PATH = "uk.ac.ebi.config/sys.properties"; 
	public final static String XML_CONFIG_FILE = "uk.ac.ebi.xml/config.xml"; 
	//public final static String XSD_SCHEMA = "xml/mainSchema.xsd"; 
}
