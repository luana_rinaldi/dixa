
package setup;
/***
 * @name DixaSetup : This class takes care to configure the initial settings for the loader
 * @version 1.0
 * @author: Luana Rinaldi
 */
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class DixaSetup {
	private static boolean initDone = false;
	public static Logger log = Logger.getLogger(DixaSetup.class); 
	
	public static void init() throws Throwable {
		if(!initDone) {
			try{
				//init Log4j (logging system)
				DOMConfigurator.configure("../dixa/"+Constant.XML_CONFIG_FILE); 		
				log.debug("[Setup - init] - log4j Configured"); 

				
				log.info("[Setup - init] Configuration Complete");
			}catch(Throwable t){
				log.fatal("Setup is not completed! cannot run process ", t);
				throw t; 
			}
			initDone = true;
		}
	}
}