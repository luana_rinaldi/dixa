package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.FileMetadata;
import models.Submission;

import org.apache.log4j.Logger;


public class DAOSubmission {
	private static Logger log = Logger.getLogger(DAOSubmission.class);
	
	private static final String query_last_sub_id= "SELECT  SUBMISSION_ID FROM submission WHERE submission_id like ? ORDER BY SUBMISSION_ID desc LIMIT 1";
	private static final String query_log_submission="INSERT INTO submission VALUES(?,?,?,?,?)";
	private static final String query_log_submission_composition="INSERT INTO submission_composition VALUES(?,?,?,?,?)";
	
	public static String getLastID(String prefix) throws Exception{
		String last= null;
		PreparedStatement pst = null; 
		ResultSet rst = null;
		Connection conn=null;
		try
		{
			conn = DixaDataSource.instance().getConnection(); 
			pst = conn.prepareStatement(DAOSubmission.query_last_sub_id);
			pst.setString(1,prefix+"_%");
			rst = pst.executeQuery(); 
			if(rst.next()){
				last = rst.getString(1);
			}
			log.debug("Last submission ID: "+last); 
		}catch(Exception e)
		{
			e.printStackTrace(); 
		}
		finally
		{
			rst.close(); 
			pst.close();
			conn.close();
		} 
		return last; 
	}
	public static boolean logSubmissionComposition(FileMetadata fm,String sub_id) throws Exception{
		
		boolean done= false; 
		PreparedStatement pst = null; 
		Connection conn = null;
		try
		{
			String sql = DAOSubmission.query_log_submission_composition;
			conn = DixaDataSource.instance().getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, fm.getName()); 
			pst.setDate(3, new java.sql.Date(fm.getCreation_date().getTime()));
			pst.setString(2,sub_id); 
			pst.setString(4, fm.getMd5()); 
			pst.setBoolean(5,fm.isStatus()); 
			
			log.debug(sql); 
			pst.executeUpdate();
			done = true;
		}catch(SQLException sqle){
			if(sqle.getErrorCode() == 942){
				//log.warn("Error Code 942"); 
			}
			log.error(sqle.getMessage());
			log.error(sqle.getErrorCode());
		}
		catch(Exception e)
		{ 
			log.fatal("Exception: ",e);
			
			throw e; 
		}
		finally
		{
			pst.close(); 
			conn.close();
		} 
		return done;
	}
	
	public static boolean logSubmission(Submission s) throws Exception{
		
		boolean done= false; 
		PreparedStatement pst = null; 
		Connection conn = null;
		try
		{
			String sql = DAOSubmission.query_log_submission;
			conn = DixaDataSource.instance().getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, s.getSubmission_id()); 
			pst.setDate(2, new java.sql.Date(s.getSubmission_date().getTime()));
			pst.setString(3,s.getPath()); 
			pst.setString(4, s.getSubmitter()); 
			pst.setInt(5,s.getNumber_of_files()); 
			
			log.debug(sql); 
			pst.executeUpdate();
			done = true;
			
			for (FileMetadata fm : s.getFiles()){
				DAOSubmission.logSubmissionComposition(fm, s.getSubmission_id());
			}
		}catch(SQLException sqle){
			if(sqle.getErrorCode() == 942){
				//log.warn("Error Code 942"); 
			}
			log.error(sqle.getMessage());
			log.error(sqle.getErrorCode());
		}
		catch(Exception e)
		{ 
			log.fatal("Exception: ",e);
			
			throw e; 
		}
		finally
		{
			pst.close(); 
			conn.close();
		} 
		return done;
	}
	
}
