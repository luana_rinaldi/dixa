package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

public class DAOLoaderLog {
	public static Logger log = Logger.getLogger(DAOLoaderLog.class);
	
	private static final String query_save_log =  "INSERT INTO loader_log(starttime,endtime,number_of_processes,submission_ids) VALUES (?,?,?,?)";
	
	public static boolean saveLog(Timestamp start, Timestamp end, int num_proc, String[] dixa_ids)
																					throws Exception{
		boolean done = false;
		PreparedStatement pst = null; 
		Connection conn = null;
		try
		{
			String sql = DAOLoaderLog.query_save_log;
			conn = DixaDataSource.instance().getConnection();
			pst = conn.prepareStatement(sql);
			pst.setTimestamp(1,start);
			pst.setTimestamp(2,end);
			pst.setInt(3, num_proc);
			pst.setObject(4, dixa_ids);
			
			log.debug(sql); 
			pst.executeUpdate();
			done = true;
		}catch(SQLException sqle){
			if(sqle.getErrorCode() == 942){
				//log.warn("Error Code 942"); 
			}
			log.error(sqle.getMessage());
			log.error(sqle.getErrorCode());
		}
		catch(Exception e)
		{ 
			log.fatal("Exception: ",e);
			
			throw e; 
		}
		finally
		{
			pst.close(); 
			conn.close();
		} 
		return done;
	}
}
