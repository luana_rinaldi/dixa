package db;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.sql.DataSource;

import models.DBProperties;

import org.apache.log4j.Logger;

import xml.ConfigParser;
import xml.DBConfigParser;


public class DixaDataSource {
	
	public static Logger log = Logger.getLogger(DataSource.class); 
	
	private DBProperties dbProp; 
	private static DixaDataSource instance = null; 
	
	
	protected DixaDataSource() {
		try{	
			ConfigParser dbConfig = new DBConfigParser();
			this.dbProp = (DBProperties)dbConfig.getProperties(); 
				
		}catch(Throwable t){
			log.fatal("Fatal Exception", t); 
		}
	}
	
	public static DixaDataSource instance(){
		if(null == instance){
			instance = new DixaDataSource(); 
		}
		return instance; 
	}
	

	
	
	public synchronized Connection getConnection() {
        Connection conn = null;
        try
        {
            String userName = this.dbProp.getUsername();
            
            String password = this.dbProp.getPassword();
            String url = this.dbProp.getDatabaseURI();
            Class.forName (this.dbProp.getJdbcDriver()).newInstance ();
            conn = DriverManager.getConnection (url, userName, password);
            log.debug ("Database connection established");
        }
        catch (Exception e)
        {
            log.error ("Cannot connect to database server ");
            e.printStackTrace(); 
            
        }
        return conn; 
	}	
	
}

