package main;

/***
 * @name MAIN : start CLASS FOR DIXA LOADER 
 * @version 1.0
 * @author: Luana Rinaldi
 */

import org.apache.log4j.Logger;

import setup.DixaSetup;
import threads.FTPManager;
import exceptions.DixaException;

/**
 * MAIN CLASS
 * @author luanarinaldi
 *
 *Parameters: 
 * args[0] = ACTION TO DO: 
 * 				'h' = print Help
 * 				's' = sync ftp folder
 * 				'n' = register new submission
 * args[1] = (IF args[0] = 's') 
 * 				1 = 'GeneData'
 * 			
 * 			 (IF args[0] = 'n'
 * 				
 * 
 */
public class Dixa {
	private static Logger log = Logger.getLogger(Dixa.class);  
	
	private static void runSync(String analyzer){
		log.info("Starting synchronization with "+analyzer);
		FTPManager.sync(analyzer);
		log.info("Done!");
		
	}
	private static void setParameters(String[] args) throws Exception{
		
		if(args != null && args.length!=1){
			String action = args[1];
			
			// PRINT HELP 
			if(action.equals("h")){
				Dixa.printHelp();
				
			// RUN SYNC
			}else if(action.equals("s")){
				int analyzerCode = Integer.parseInt(args[2]); 
				String analyzer = null;
				switch (analyzerCode){
					case 1: analyzer = "GeneData";	
				}
				Dixa.runSync(analyzer);	
			}
		}else{
			log.debug("Parameters required, use \"h\" for help");
			throw new DixaException("Parameters required, use \"h\" for help");
		}

	}
	
	private static void printHelp(){
		log.info("*** HELP FOR DIXA LOADER ***\n " +
				"Parameters:\n  " +
				"\t args[0] = ACTION TO DO:\n " +
				"\t\t 'h' = print Help\n" +
				"\t\t 's' = sync ftp folder\n" +
				"\t args[1] = (IF args[0] = 's')\n " +
				"\t\t  1 = 'GeneData'\n");
	}
	
	private static void run(String[] args){
		try {
			//setting up the initial configuration 
			log.info("********** DIXA LOADER **********"); 
			DixaSetup.init();	
			log.info("Loader Setup Complete");
			
			Dixa.setParameters(args);

			//Connection conn = DixaDataSource.instance().getConnection(); 
			//conn.close();
		} 
		catch (Exception e) {
			log.fatal(e.getMessage());
			log.fatal("Process Aborted!"); 
			System.exit(0); 
		}catch (Throwable t) {
			t.printStackTrace();
			log.fatal(t.getCause());
			log.fatal("Process Aborted!"); 
			System.exit(0); 
		} finally{
			System.exit(0); 
		}	
	}
	public static void main(String[] args) {
		args = new String[10];
		args[1]="s";
		args[2]="1";
		Dixa.run(args);
	}
}
