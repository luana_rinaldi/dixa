package threads;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import managers.IDGenerator;
import models.FTPProperties;
import models.FileMetadata;
import models.Submission;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.log4j.Logger;

import xml.ConfigParser;
import xml.FTPConfigParser;
import db.DAOLoaderLog;
import db.DAOSubmission;
/**
 * FTP Synchronization
 * @author luanarinaldi
 *
 */
public class FTPManager {
	public static Logger log = Logger.getLogger(FTPManager.class); 
	 
	public static void sync(String analyser){
		
		//start time of the process
		java.sql.Date now = new java.sql.Date(new java.util.Date().getTime());
		
		String sourceFolder = null;
		String destinationFolder = null; 
		int jobs = 0; 
		
		
		ConfigParser parser;
		 
		try {
			parser = new FTPConfigParser();
			FTPProperties ftpProp =(FTPProperties) parser.getProperties();
			
			Submission s = null;
			//Step 1: checking new uploads on the FTP from submitters
			String way = "SUB"; 
			s = FTPManager.createSubmission(ftpProp, jobs, analyser, s, way );
			DAOSubmission.logSubmission(s);
			
			//STEP2 : check theresult folder
			way = "ANL";
			s = FTPManager.createSubmission(ftpProp, jobs, analyser, s, way );
					
			DAOSubmission.logSubmission(s);
	        
		    
			//end of the process
		    java.sql.Date end = new java.sql.Date(new java.util.Date().getTime());
		    
		    //save log for this run
		    DAOLoaderLog.saveLog(new Timestamp(now.getTime()), new Timestamp(end.getTime()), jobs, new String[10]);
		}catch (IOException ioe) {
			log.error(ioe.getMessage());
			ioe.printStackTrace();
		} 
		catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	private static Submission createSubmission(FTPProperties ftpProp,int jobs, 
											String analyser, Submission s, String way) throws IOException, InterruptedException{
		String[] children = FTPManager.getChildren(ftpProp.getUploadFolder());
		
		if (children != null) {
		    jobs++;
		    
			for (int i=0; i<children.length; i++) {
		    	
		        // Get filename of directory: it will be the name of the submitter
		        String submitter = children[i];
		        
		        String sourceFolder = null; 
		        String destinationFolder = null; 
		        String id = null;
		        
		        //generation of the submission ID
		        id = IDGenerator.getNewID(way); 
		        
		        if (way.equals("SUB") ){
			        //build the path of the new sumbission
			        sourceFolder = ftpProp.getUploadFolder()+"/"+submitter+"/"+ftpProp.getNewSubFolder()+"/";
			           		        			        
			        //sync folder
			        //destinationFolder = ftpProp.getLoadFolder()+"/"+ftpProp.getAnalyzers().get(analyser)+"/"+ftpProp.getArchiveFolder()+"/"+id;
			        destinationFolder = ftpProp.getUploadFolder()+"/"+submitter+"/"+ftpProp.getArchiveFolder()+"/"+id;
			        String symlink = ftpProp.getLoadFolder()+"/"+ftpProp.getAnalyzers().get(analyser)+"/"+ftpProp.getArchiveFolder()+"/"+id;
			        FTPManager.createSymLink(symlink, destinationFolder);
		        }else{
			        //build the path of the new results
			         sourceFolder = ftpProp.getLoadFolder()+"/"+analyser+"/"+ftpProp.getResultsFolder()+"/"+ftpProp.getLoad_newResultsFolder();
			        			        
			        //sync folder
			        //destinationFolder = ftpProp.getLoadFolder()+"/"+ftpProp.getAnalyzers().get(analyser)+"/"+ftpProp.getArchiveFolder()+"/"+id;
			        destinationFolder = ftpProp.getLoadFolder()+"/"+analyser+"/"+ftpProp.getLoadArchiveFolder()+"/"+id;
		        }
		        s = new Submission(id,submitter);
		        s = FTPManager.syncFolders(analyser, sourceFolder, destinationFolder, s); 
			}
		}
		return s; 
	}
	//TODO: Change with something less expensive in memory
	private static void createSymLink(String simLink, String ref) throws IOException, InterruptedException{
		Process process = Runtime.getRuntime().exec( new String[] { "ln", "-s", ref, simLink } );
		process.waitFor();
		process.destroy();
	}
	
	private static String[] getChildren(String filename){
		File f = new File(filename);
		//children 
		return f.list();
	}
	private static Submission syncFolders(String analyser, String sourceFolder, String destinationFolder, Submission s){

		
		//List to store the submission id worked by the loader in this run
		List<String> ids = new ArrayList<String>();
		
		try{
			        
			int file_counter = 0; 
			        
	        Date submission_date = null; 
	        //move files into load folder
	        for (File f2: FileUtils.listFiles(new File(sourceFolder),FileFileFilter.FILE,null)){
	        	
	        	submission_date = new Date(f2.lastModified());
	        	s.setSubmission_date(submission_date); 
	        	
	        	String md5 = DigestUtils.md5Hex(new FileInputStream(f2));
	        	
	        	FileMetadata file = new FileMetadata(f2.getName(), submission_date, md5, true); 

	        	FileUtils.moveFileToDirectory(new File(sourceFolder+'/'+f2.getName()),new File( destinationFolder), true);
	        	
	        	s.addFileMetadata(file); 
	        	file_counter++; 
	        	
	        }
	        
	        s.setNumber_of_files(file_counter); 

	
		}catch(IllegalArgumentException iae){
			log.error(iae.getMessage());
			iae.printStackTrace();
		}
		catch (IOException ioe) {
			log.error(ioe.getMessage());
			ioe.printStackTrace();
		} 
		catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return s;
	}
	
	public static void main(String[] args) {
		FTPManager.sync("GeneData");
	}
}
