package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Submission {

	private String submission_id;
	private Date submission_date;
	private String path;
	private String submitter;
	private int number_of_files; 
	
	private List<FileMetadata> files; 


	public Submission(String submission_id, Date submission_date, String path, 
			String submitter, int number_of_files) {
		super();
		this.submission_id = submission_id;
		this.submission_date = submission_date;
		this.path = path;
		this.submitter = submitter;
		this.number_of_files = number_of_files;
		this.files = new ArrayList<FileMetadata>(); 
	}

	public Submission(String submission_id, 
			String submitter) {
		super();
		this.submission_id = submission_id;
		this.submission_date = null;
		this.path = null;
		this.submitter = submitter;
		this.number_of_files = -1;
		this.files = new ArrayList<FileMetadata>(); 
	}
	
	
	public String getSubmission_id() {
		return submission_id;
	}

	public void addFileMetadata(FileMetadata fm){
		if (this.files== null){
			this.files = new ArrayList<FileMetadata>(); 
		}
		this.files.add(fm);
	}

	public List<FileMetadata> getFiles() {
		return files;
	}

	public void setFiles(List<FileMetadata> files) {
		this.files = files;
	}

	public void setSubmission_id(String submission_id) {
		this.submission_id = submission_id;
	}

	public Date getSubmission_date() {
		return submission_date;
	}

	public void setSubmission_date(Date submission_date) {
		this.submission_date = submission_date;
	}


	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public int getNumber_of_files() {
		return number_of_files;
	}

	public void setNumber_of_files(int number_of_files) {
		this.number_of_files = number_of_files;
	}
	
	

}
