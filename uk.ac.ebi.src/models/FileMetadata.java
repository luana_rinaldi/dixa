package models;

import java.util.Date;

public class FileMetadata {

	private String name; 
	private Date creation_date; 
	private String md5; 
	private boolean status;
	public FileMetadata(String name, Date creation_date, String md5,
			boolean status) {
		super();
		this.name = name;
		this.creation_date = creation_date;
		this.md5 = md5;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	} 
	
}
