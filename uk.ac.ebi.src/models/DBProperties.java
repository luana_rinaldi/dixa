package models;
/***
 * @name DBProperties
 * @version 1.0
 * @author: Luana Rinaldi
 */


public class DBProperties extends Property{
	private String alias; 
	private String jdbcDriver; 
	private String databaseURI; 
	private String username; 
	private String password; 

	
	
	public DBProperties( String alias, String jdbcDriver, String databaseURI,
			String username, String password) {
		super();
		this.alias = alias; 
		this.jdbcDriver = jdbcDriver;
		this.databaseURI = databaseURI;
		this.username = username;
		this.password = password;
	
	}


	public String getAlias() {
		return alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getJdbcDriver() {
		return jdbcDriver;
	}


	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}


	public String getDatabaseURI() {
		return databaseURI;
	}


	public void setDatabaseURI(String databaseURI) {
		this.databaseURI = databaseURI;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}	
	public String toString(){
		return this.alias+" "+this.jdbcDriver+" "+this.databaseURI+" "+this.username+" "+this.password; 
	}
}

