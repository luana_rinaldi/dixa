package models;

import java.util.Map;

public class FTPProperties extends Property {

	private String ftp_realPath;
	private String uploadFolder;
	private String archiveFolder;
	private String newSubFolder;
	private String loadFolder;
	private String syncFolder;
	private String resultsFolder;
	private String load_newResultsFolder; 
	private String loadArchiveFolder; 
	
	private Map<String,String> analyzers;
	
	public FTPProperties(String ftp_realPath, String uploadFolder,
			String archiveFolder, String newSubFolder, String loadFolder,String syncFolder,
			String resultsFolder, String load_newResultsFolder, String loadArchiveFolder,
			Map<String, String> analyzers) {
		super();
		this.ftp_realPath = ftp_realPath;
		this.uploadFolder = ftp_realPath+"/"+uploadFolder;
		this.loadFolder = ftp_realPath+"/"+loadFolder;
		this.archiveFolder = archiveFolder;
		this.newSubFolder = newSubFolder;
		this.syncFolder = syncFolder;
		this.resultsFolder = resultsFolder;
		this.analyzers=analyzers;
		this.load_newResultsFolder = load_newResultsFolder; 
		this.loadArchiveFolder= loadArchiveFolder; 
	}
	
	
	public String getLoad_newResultsFolder() {
		return load_newResultsFolder;
	}


	public void setLoad_newResultsFolder(String load_newResultsFolder) {
		this.load_newResultsFolder = load_newResultsFolder;
	}


	public String getLoadArchiveFolder() {
		return loadArchiveFolder;
	}


	public void setLoadArchiveFolder(String loadArchiveFolder) {
		this.loadArchiveFolder = loadArchiveFolder;
	}


	public Map<String, String> getAnalyzers() {
		return analyzers;
	}
	public void setAnalyzers(Map<String, String> analyzers) {
		this.analyzers = analyzers;
	}
	public String getLoadFolder() {
		return loadFolder;
	}
	public void setLoadFolder(String loadFolder) {
		this.loadFolder = loadFolder;
	}
	public String getFtp_realPath() {
		return ftp_realPath;
	}
	public void setFtp_realPath(String ftp_realPath) {
		this.ftp_realPath = ftp_realPath;
	}
	public String getUploadFolder() {
		return uploadFolder;
	}
	public void setUploadFolder(String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}
	public String getArchiveFolder() {
		return archiveFolder;
	}
	public void setArchiveFolder(String archiveFolder) {
		this.archiveFolder = archiveFolder;
	}
	public String getNewSubFolder() {
		return newSubFolder;
	}
	public void setNewSubFolder(String newSubFolder) {
		this.newSubFolder = newSubFolder;
	}
	public String getSyncFolder() {
		return syncFolder;
	}
	public void setSyncFolder(String syncFolder) {
		this.syncFolder = syncFolder;
	}
	public String getResultsFolder() {
		return resultsFolder;
	}
	public void setResultsFolder(String resultsFolder) {
		this.resultsFolder = resultsFolder;
	}
	
	
	
}
