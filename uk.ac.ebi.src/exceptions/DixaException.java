package exceptions;

public class DixaException extends Exception {
	private static final long serialVersionUID = 1L;
		
		public DixaException() {
			super();
		}
		
		public DixaException(String message) {
			super(message);
		}

		public DixaException(Throwable cause) {
			super(cause); 
		}
		
}
