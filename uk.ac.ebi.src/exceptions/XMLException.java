package exceptions;

/***
 * @name XMLException 
 * @version 1.0
 * @author: Luana Rinaldi
 */

public class XMLException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public XMLException() {
		super();
	}
	
	public XMLException(String message) {
		super(message);
	}

	public XMLException(Throwable cause) {
		super(cause); 
	}
	
}
