version 1.0
-----------

FTP folder structure
====================

ftp root path: /nfs/ftp/private/dixa_ftp/

upload
		<submitters>
				archive
				new_submission
					<file1>
					<file2>

load 
		GeneData
				results
					archive
					new_results
						<res_file1>
						<res_file2>
				sync
					<ID>
						<file1>
						<file2>

Description of flow
===================
Submitter logs in to FTP (ftp user: dixa_ftp) and drops files under the "new_submission"
folder of his/her "<submitters>" folder. A demon running the loader checks for files in
each of the "new_submission" folders. If there are any files, it creates an entry in the
"submission" and "submission_composition" table of the loader database (see next section), 
and moves the files to the "<submitters>/archive" folder, under a new folder <SUBMISSION_ID>. 
The loader will create a symbolic link between the "archive" and the "sync" of the analyser.
("GeneData" is currently the only analyser).

The analyser (GeneData) drops processed files in "new_results". The loader checks whether there
are any files in the "new_results" folder. If so, it moves them to the "archive" of "results"
under a new folder <ID> (this is different than the <ID> in the "sync" folder). The loader
will register this operation in the "sumbission" and "sumbission_composition" table. 

We de-couple the submission of files from "sumbitters" and "analysers" (i.e. use a different <ID>)
because analysers might use a different "batch" of submitted files for analysis. Also, analysers
might want to perform different analyses on the same data, in which case we should not overwrite
files coming from the same original source.

The way to connect submitted data to analysed data will be by parsing the actual ISA-tab files,
which will be done in the future.



A submission <ID>_receipt.xml
is also created. This file contains the <ID>, a list of the filenames and their md5
sums, the date & time of submission and the name of the "<submitters>" folder.


Database (name of the tables to be checked) 

========

"sumbission" table 
  `submission_id` //<SUBMISSION_ID>
  `submission_date` // date of the last modification in the file
  `path` // folder where the bunch of file is stored
  `submitter` 
  `number_of_files` //number of files of the submission
  
  
"submission_composition" table
  `file_name` 
  `submission_id` // reference to the submission table
  `creation_date` 
  `md5` 
  `status`

  "loader_log" table
    `job_id`
  `starttime`
  `endtime`
  `number_of_processes` 
  `submission_ids` //ids worked in this run, as a coma-separated string